<?php
    // DB params
    define('DB_HOST', 'localhost');
    define('DB_USER', 'root');
    define('DB_PASS', '');
    define('DB_NAME', 'scandiwebdb');
    
    // App Root
    define('APPROOT', dirname(dirname(__FILE__)));
    // URL Root
    define('URLROOT', 'http://localhost/scandiweb');
    // Site Name
    define('SITENAME', 'ScandiWeb');
    // App Version
    define('APPVERSION', '1.0.0');