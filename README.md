# README

**Developer task for ScandiWeb**

## About project

    This is a simple PHP MVC project that I made as developer task for ScandiWeb.
    Project has MVC structure with CRUD operations where you can create, read, and delete products.
    
## How to use

        - To run this project locally you will need local web server. I use XAMPP, 
    but there are different software stacks that you can use 
    (https://en.wikipedia.org/wiki/List_of_Apache%E2%80%93MySQL%E2%80%93PHP_packages)
        - Open XAMPP, start your Apache and MySQL modules.
        - Open phpmyadmin (http://localhost/phpmyadmin/).
        - Create database named 'scandiwebdb', or you can choose whatever name you want, 
    but then you will have to change app/config/config.php file accordingly.
        - Select your database and import products.sql file found in app/data/products.sql
        - Now you can access the site trought your localhost connection.
    
## Built With

- PHP
- Bootstrap4
- HTML/CSS/JavaScript
- No frameworks - PLAIN PHP

## Author

👤 **Peteris**

- Github: [@Peteris](https://github.com/PeterisZevalds)
